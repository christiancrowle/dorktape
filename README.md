# Dorktape!

Dorktape is a fork of [Duktape](https://github.com/svaarala/duktape) 2.x, created for [POBARISNA](https://gitlab.com/christiancrowle/pobarisna).

I decided to fork Duktape for two reasons:

1. I wanted to make major code changes to allow for better control over execution. These changes were promised for Duktape 3.x, but Duktape development has massively slowed. If the changes were made already, they aren't documented, and I'd rather make sure I have 100% control anyway.
2. Duktape's build system **SUCKS** unfortunately. The project is fantastic! It does exactly what I need it to do. But making any code changes to 2.x through the "official" means involves ancient and massively complex python scripts that generate the amalgamated source. 3.x has a similar issue, just with Node.js instead.

Thus, the fork. The aim is for a focus on unamalgamated sources, built like a traditional static library. This allows for easier hacking on the source, and makes everything a little easier to digest (and no more spaghetti build system!). This of course has downsides, such as losing automatic builtin generation. I'll come up with a solution to that when I need it.

## Building/Using

The goal is for Dorktape to function almost identically to Duktape 2.x in almost every way. However, the build process is obviously different.

To build Dorktape, you'll need Rake and a C compiler. Thankfully, Dorktape itself has no dependencies, so a simple

`$ rake`

should produce a static library `libdork.a` in the `build/` directory. Link this into your project, add the `dorktape/` folder as an include directory, and everything should "just work".

## License

Like Duktape (and POBARISNA), Dorktape is released under the permissive MIT license. See `LICENSE.txt` for details.

Lastly, this project would not be possible without the original Duktape project by Sami Vaarala et al. See ORIGINAL_AUTHORS.rst for a full list of the original Duktape contributors.
